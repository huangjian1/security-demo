package com.pers.securitydemo.service;

import com.pers.securitydemo.DemoApplicationTests;
import com.pers.securitydemo.model.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class UserServiceTest extends DemoApplicationTests {

    @Autowired
    private IUserService userService;

    @Test
    public void getUserByName(){
        User userByName = userService.getUserByName("test");
        System.out.println(userByName);
    }

}
