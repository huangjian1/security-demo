package com.pers.securitydemo;

import com.pers.securitydemo.jwt.JwtTokenGenerator;
import com.pers.securitydemo.jwt.JwtTokenPair;
import com.pers.securitydemo.mapper.UserMapper;
import com.pers.securitydemo.model.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tk.mybatis.mapper.entity.Example;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private JwtTokenGenerator jwtTokenGenerator;


    @Test
    public void test(){
        User user =new User();
        user.setUserName("test");
        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userName",user.getUserName());
        List<User> users = userMapper.select(user);
        System.out.println(users);
    }


    @Test
    void contextLoads() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String master = passwordEncoder.encode("test");
        System.out.println(master);
    }

    @Test
    public void jwtTest(){
        HashSet<String> roles = new HashSet<>();
        HashMap<String, String> additional = new HashMap<>();
        additional.put("uname","Felordcn");

        JwtTokenPair jwtTokenPair = jwtTokenGenerator.jwtTokenPair("133", roles, additional);

        System.out.println("jwtTokenPair = " + jwtTokenPair);
    }

    @Test
    public void jwtTest1(){
        System.out.println(jwtTokenGenerator.decodeAndVerify("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhbGwiLCJ1bmFtZSI6IkZlbG9yZGNuIiwicm9sZXMiOiJbXSIsImlzcyI6ImZlbG9yZC5jbiIsImF1ZCI6IjEzMyIsImV4cCI6IjIwMjAtMDUtMTggMTc6MDM6MzIiLCJpYXQiOiIyMDIwLTA0LTE4IDE3OjAzOjMyIiwianRpIjoiZDEyNmJhM2E4OGQ0NDcxMzgyNTFkYzRlZTcyNGZmZDgifQ.KO_c6D7Ol4kc4sd21r0o5mQKjOv6B0VCFSPe5jyRKwAYw3OuAk5JTTs5y6K1Sw3fLGUvdI0PlbF2TpkGn9IFLRRIPXhQvQda7PIiES_00Wn29GaPGi1K-0OrQROBOc6LJl6MlxKng_efH9qzqY_NgvehIzQFrzr080Hg_Z7PZ0k"));
    }

}
