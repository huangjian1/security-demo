package com.pers.securitydemo.controller;

import cn.hutool.json.JSONObject;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/notAuth")
public class NotAuthController {

    /**
     *
     * 拥有ROLE_USER方法才能够调用此方法，此注解不允许对一个方法设置多个权限
     * @Secured("ROLE_USER")
     *
     * 拥有ROLE_USER权限或者ROLE_ADMIN权限才能够调用此方法
     * @PreAuthorize("hasAnyRole('USER','ADMIN')")
     *
     * 同时拥有ROLE_ADMIN权限与ROLE_USER权限才能够调用此方法，支持对一个方法设置多个权限
     * @PreAuthorize("hasAuthority('ADMIN') AND hasAuthority('USER')")
     */

    @GetMapping("/get")
    @PreAuthorize("hasAuthority('ADMIN') AND hasAuthority('USER')")
    public JSONObject hello(){
        Map<String,Object> params = new HashMap<>();
        params.put("code",200);
        params.put("msg","this is notAuth");
        return new JSONObject(params);
    }
}
