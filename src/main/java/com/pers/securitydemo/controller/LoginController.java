package com.pers.securitydemo.controller;

import com.pers.securitydemo.model.pojo.JSONResult;
import com.pers.securitydemo.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/login",produces="application/json;charset=UTF-8")
@Slf4j
public class LoginController {

    @Resource
    private IUserService userService;

    @PostMapping("/failure")
    @ResponseBody
    public JSONResult loginFailure() {
        log.info("failure");
        return JSONResult.notFound("not found user", null);
    }

    @PostMapping("/success")
    @ResponseBody
    public JSONResult loginSuccess(){
        log.info("success");
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = principal.getUsername();
        com.pers.securitydemo.model.pojo.User userByName = userService.getUserByName(userName);
        return JSONResult.success("ok",userByName);
    }



}
