package com.pers.securitydemo.controller;

import cn.hutool.json.JSONObject;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/welcome")
    public JSONObject welcome(){
        Map<String,Object> params = new HashMap<>();
        params.put("code",200);
        params.put("msg","this is admin welcome");
        return new JSONObject(params);
    }


}
