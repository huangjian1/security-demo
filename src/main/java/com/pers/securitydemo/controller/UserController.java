package com.pers.securitydemo.controller;

import cn.hutool.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/welcome")
    public JSONObject welcome(){
        Map<String,Object> params = new HashMap<>();
        params.put("code",200);
        params.put("msg","this is welcome");
        return new JSONObject(params);
    }

    @GetMapping("/details")
    public JSONObject details(){
        Map<String,Object> params = new HashMap<>();
        params.put("code",200);
        params.put("msg","this is details");
        return new JSONObject(params);
    }
}
