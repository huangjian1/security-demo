package com.pers.securitydemo.model.pojo;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class JSONResult {

    private Integer code;

    private String msg;

    private Object data;

    public JSONResult(Integer code,String msg,Object data
    ){
    this.code=code;
    this.msg=msg;
    this.data=data;
    }

    public static JSONResult success(String msg,Object data){
        return new JSONResult(200,msg,data);
    }

    public static JSONResult error(String msg,Object data){
        return new JSONResult(500,msg,data);
    }

    public static JSONResult notFound(String msg,Object data){
        return new JSONResult(404,msg,data);
    }

    public static JSONResult unauthorized(String msg,Object data){
        return new JSONResult(HttpStatus.UNAUTHORIZED.value(),msg,data);
    }
}
