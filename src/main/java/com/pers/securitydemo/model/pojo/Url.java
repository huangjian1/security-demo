package com.pers.securitydemo.model.pojo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author huagnjian
 */
@Data
@Table(name = "t_url")
public class Url {

    @Id
    private Integer id;

    @Column(name = "url_desc")
    private String urlDesc;

    @Column(name = "url")
    private String url;
}
