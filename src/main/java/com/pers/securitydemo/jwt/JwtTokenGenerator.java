package com.pers.securitydemo.jwt;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.RsaSigner;
import org.springframework.security.jwt.crypto.sign.RsaVerifier;
import org.springframework.security.jwt.crypto.sign.SignatureVerifier;
import org.springframework.util.Assert;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;

/**
 * JwtTokenGenerator
 *
 * @author Felordcn
 * @since 15 :26 2019/10/25
 */
@Slf4j
public class JwtTokenGenerator {


    private static final String JWT_EXP_KEY = "exp";

    //JwtPayload构造器
    private JwtPayloadBuilder jwtPayloadBuilder = new JwtPayloadBuilder();

    //Jwt配置文件
    private JwtProperties jwtProperties;

    //token储存
    private JwtTokenStorage jwtTokenStorage;

    //公私钥
    private KeyPair keyPair;

    /**
     * Instantiates a new Jwt token generator.
     *
     * @param jwtTokenStorage the jwt token storage
     * @param jwtProperties   the jwt properties
     */
    public JwtTokenGenerator(JwtTokenStorage jwtTokenStorage, JwtProperties jwtProperties) {
        this.jwtTokenStorage = jwtTokenStorage;
        this.jwtProperties = jwtProperties;

        KeyPairFactory keyPairFactory = new KeyPairFactory();
        //秘钥对
        //getKeyLocation key位置
        //keyAlias 别名
        //keyPass 别名密码

        //第一步初始化公私钥，从配置文件中获取路径、别名、别名密码、然后生成公钥和私钥
        this.keyPair = keyPairFactory.create(jwtProperties.getKeyLocation(), jwtProperties.getKeyAlias(), jwtProperties.getKeyPass());
    }


    /**
     * Jwt token pair jwt token pair.
     *
     * @param aud        the aud
     * @param roles      the roles
     * @param additional the additional
     * @return the jwt token pair
     */
    public JwtTokenPair jwtTokenPair(String aud, Set<String> roles, Map<String, String> additional) {
        //aud 接收jwt的一方
        //accessExpDays 有效期
        //roles 权限集合
        //additional 额外的数据

        //第一步：构建payload
        //第二步：获取私钥创建签名
        String accessToken = jwtToken(aud, jwtProperties.getAccessExpDays(), roles, additional);
        String refreshToken = jwtToken(aud, jwtProperties.getRefreshExpDays(), roles, additional);

        JwtTokenPair jwtTokenPair = new JwtTokenPair();
        jwtTokenPair.setAccessToken(accessToken);
        jwtTokenPair.setRefreshToken(refreshToken);

        // 放入缓存
        jwtTokenStorage.put(jwtTokenPair, aud);

        return jwtTokenPair;
    }


    /**
     * Jwt token string.
     *
     * @param aud        the aud
     * @param exp        the exp
     * @param roles      the roles
     * @param additional the additional
     * @return the string
     */
    private String jwtToken(String aud, int exp, Set<String> roles, Map<String, String> additional) {
        String payload = jwtPayloadBuilder
                //jwt签发者
                .iss(jwtProperties.getIss())
                //jwt所面向的用户
                .sub(jwtProperties.getSub())
                //接收jwt的一方
                .aud(aud)
                //额外的数据
                .additional(additional)
                //角色集合
                .roles(roles)
                //access jwt token 过期时间
                .expDays(exp)
                .builder();

        //获取私钥
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        RsaSigner signer = new RsaSigner(privateKey);
        return JwtHelper.encode(payload, signer).getEncoded();
    }


    /**
     * 解码 并校验签名 过期不予解析
     *
     * @param jwtToken the jwt token
     * @return the jwt claims
     */
    public JSONObject decodeAndVerify(String jwtToken) {
        Assert.hasText(jwtToken, "jwt token must not be bank");
        RSAPublicKey rsaPublicKey = (RSAPublicKey) this.keyPair.getPublic();
        SignatureVerifier rsaVerifier = new RsaVerifier(rsaPublicKey);
        Jwt jwt = JwtHelper.decodeAndVerify(jwtToken, rsaVerifier);
        String claims = jwt.getClaims();
        JSONObject jsonObject = JSONUtil.parseObj(claims);
        String exp = jsonObject.getStr(JWT_EXP_KEY);

        if (isExpired(exp)) {
            throw new IllegalStateException("jwt token is expired");
        }
        return jsonObject;
    }

    /**
     * 判断jwt token是否过期.
     *
     * @param exp the jwt token exp
     * @return the boolean
     */
    private boolean isExpired(String exp) {

        return LocalDateTime.now().isAfter(LocalDateTime.parse(exp, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

}