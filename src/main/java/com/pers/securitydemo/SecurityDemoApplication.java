package com.pers.securitydemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.CustomAutowireConfigurer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;


/**
 * @author huangjian
 */
@EnableWebSecurity
@SpringBootApplication
@MapperScan(basePackages = {"com.pers.securitydemo.mapper"})
//开启注解
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
//设置缓存
@EnableCaching
public class SecurityDemoApplication {

   //@Autowired
    //private DataSource dataSource;

   /* @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;*/

   /* String pwdQuery = " select user_name,pwd,available from t_user where user_name = ?";

    String roleQuery = "select u.user_name, r.role_name from t_user u,t_user_role ur,t_role r " +
            "where u.id = ur.user_id and r.id = ur.role_id and u.user_name = ?";
    */

    /*@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);

        *//*auth.inMemoryAuthentication()
                //设置密码编码器
                .passwordEncoder(passwordEncoder)
                //注册用户 密码为master 赋予admin user角色权限
                .withUser("admin")
                .password(passwordEncoder.encode("123456"))
                .roles("USER","ADMIN")
                //连接方法
                .and()
                //注册用户myuser 密码 master 赋予user 角色权限
                .withUser("myuser")
                .password(passwordEncoder.encode("123456"))
                .roles("USER");*//*

        //使用内存存储
        *//*InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> userConfig = auth.inMemoryAuthentication().passwordEncoder(passwordEncoder);
        auth.jdbcAuthentication()
                .passwordEncoder(passwordEncoder)
                .dataSource(dataSource)
                .usersByUsernameQuery(pwdQuery)
                .authoritiesByUsernameQuery(roleQuery);*//*

        *//*userConfig
                //注册用户 密码为master 赋予admin user角色权限
                .withUser("admin")
                .password(passwordEncoder.encode("master"))
                .authorities("USER", "ADMIN");

        userConfig
                //注册用户myuser 密码 master 赋予user 角色权限
                .withUser("myuser")
                .password(passwordEncoder.encode("master"))
                .roles("USER");*//*

    }*/


    /*@Override
    protected void configure(HttpSecurity http) throws Exception {
//        Customizer customizer
//        FormLoginConfigurer<HttpSecurity> formLoginConfigurer = new FormLoginConfigurer<>();
//
//        http.formLogin(formLoginConfigurer);
        //关闭csrf
        http.csrf().disable()
                //开启跨域
                .cors()
                .and()
                //authorizeRequests 限定通过签名的请求
                //anyRequest 限定任意的请求
                //authenticated
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .formLogin()
                .loginProcessingUrl("/process")
                .successForwardUrl("/login/success").
                failureForwardUrl("/login/failure");

       *//* http.authorizeRequests()
                //正则匹配
                //.regexMatchers('/admin/.*').hasAuthority("")
                // 限定 /user/welcome 请求赋予角色USER或者ADMIN
                .antMatchers("/user/welcome","/user/details").hasAnyRole("USER","ADMIN")
                // 限定 /admin/ 下所有请求权限赋予角色ADMIN
                .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
                //要求ROLE_ADMIN权限以及完整登陆非记住我登录
                .antMatchers("/admin/welcome1").access("hasAuthority('ROLE_ADMIN') && ifFullyAuthenticated()")
                .antMatchers("/admin/welcome2").access("hasAuthority('ROLE_ADMIN')")
                // 其他路径允许签名后访问
                .anyRequest().permitAll()
                // 使用记住我的功能
                .and().rememberMe()

                //and连接  对没有配置权限的其他请求允许匿名访问
                .and().anonymous()

                //使用Spring Security默认的登陆页面
                .and().formLogin()
                //启动http基础验证
                .and().httpBasic().realmName("my-basic-name");*//*
    }*/

    public static void main(String[] args) {
        SpringApplication.run(SecurityDemoApplication.class, args);
    }

}
