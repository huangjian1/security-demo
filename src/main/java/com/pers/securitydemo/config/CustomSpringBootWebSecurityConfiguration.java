package com.pers.securitydemo.config;

import com.pers.securitydemo.exception.SimpleAccessDeniedHandler;
import com.pers.securitydemo.exception.SimpleAuthenticationEntryPoint;
import com.pers.securitydemo.filters.JwtAuthenticationFilter;
import com.pers.securitydemo.filters.PreLoginFilter;
import com.pers.securitydemo.jwt.JwtTokenGenerator;
import com.pers.securitydemo.jwt.JwtTokenStorage;
import com.pers.securitydemo.myHandlers.CustomLogoutHandler;
import com.pers.securitydemo.myHandlers.CustomLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@ConditionalOnClass(WebSecurityConfigurerAdapter.class)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class CustomSpringBootWebSecurityConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public PreLoginFilter preLoginFilter() {
        return new PreLoginFilter("/process", null);
    }

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter(JwtTokenGenerator jwtTokenGenerator, JwtTokenStorage jwtTokenStorage) {
        return new JwtAuthenticationFilter(jwtTokenGenerator, jwtTokenStorage);
    }

    @Bean
    public CustomLogoutSuccessHandler customLogoutSuccessHandler(JwtTokenGenerator jwtTokenGenerator, JwtTokenStorage jwtTokenStorage){
        return new CustomLogoutSuccessHandler(jwtTokenGenerator,jwtTokenStorage);
    }

    @Configuration
    @Order(SecurityProperties.BASIC_AUTH_ORDER)
    static class DefaultConfigurerAdapter extends WebSecurityConfigurerAdapter {

        @Override
        public void configure(WebSecurity web) throws Exception {
            super.configure(web);
        }

        @Autowired
        private AuthenticationFailureHandler authenticationFailureHandler;

        @Autowired
        private AuthenticationSuccessHandler authenticationSuccessHandler;

        @Qualifier("userDetailsServiceImpl")
        @Autowired
        private UserDetailsService userDetailsService;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Autowired
        private PreLoginFilter preLoginFilter;

        @Autowired
        private JwtAuthenticationFilter jwtAuthenticationFilter;

        @Autowired
        private CustomLogoutSuccessHandler customLogoutSuccessHandler;

        @Autowired
        private FilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource;

        @Autowired
        private AccessDecisionManager accessDecisionManager;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            //配置登陆的service从库中获取用户与roles，以及密码编码器
            auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            /*http.csrf().disable()
                    .cors()
                    .and()
                    .addFilterBefore(preLoginFilter(),UsernamePasswordAuthenticationFilter.class)
                    .authorizeRequests().anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginProcessingUrl("/process")
                    .successForwardUrl("/login/success").
                    failureForwardUrl("/login/failure");*/
            http.csrf().disable()
                    .cors()
                    .and()
                    // session 生成策略用无状态策略
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    //认证失败时的异常处理
                    .exceptionHandling().accessDeniedHandler(new SimpleAccessDeniedHandler()).authenticationEntryPoint(new SimpleAuthenticationEntryPoint())
                    .and()
                    //获取资源元数据对应的roles，进行比较
                    .authorizeRequests().anyRequest().authenticated().withObjectPostProcessor(myFilterSecurityInterceptorObjectPostProcessor())
                    .and()
                    //前置登陆过滤器，用于传递其他发送方式的数据
                    .addFilterBefore(preLoginFilter, UsernamePasswordAuthenticationFilter.class)
                    // jwt 必须配置于 UsernamePasswordAuthenticationFilter 之前
                    // 验证token，通过则往SecurityContext中存入用户
                    .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                    //登录成功与失败的处理器  成功后返回jwt token  失败后返回 错误信息
                    .formLogin().loginProcessingUrl("/process").successHandler(authenticationSuccessHandler).failureHandler(authenticationFailureHandler)
                    .and()
                    //配置注销过程处理器，以及注销成功处理器
                    .logout().addLogoutHandler(new CustomLogoutHandler()).logoutSuccessHandler(customLogoutSuccessHandler);
        }

        /**
         * 自定义 FilterSecurityInterceptor  ObjectPostProcessor 以替换默认配置达到动态权限的目的
         *
         * @return ObjectPostProcessor
         */
        private ObjectPostProcessor<FilterSecurityInterceptor> myFilterSecurityInterceptorObjectPostProcessor() {
            return new ObjectPostProcessor<FilterSecurityInterceptor>() {
                @Override
                public <O extends FilterSecurityInterceptor> O postProcess(O object) {
                    object.setAccessDecisionManager(accessDecisionManager);
                    object.setSecurityMetadataSource(myFilterInvocationSecurityMetadataSource);
                    return object;
                }
            };
        }

    }


}
