package com.pers.securitydemo.myHandlers;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.pers.securitydemo.jwt.JwtTokenGenerator;
import com.pers.securitydemo.jwt.JwtTokenStorage;
import com.pers.securitydemo.model.pojo.JSONResult;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;

/**
 * 自定义注销成功处理
 *
 * @author huagnjian
 */

public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {
    private static final String AUTHENTICATION_PREFIX = "Bearer ";

    private JwtTokenGenerator jwtTokenGenerator;

    private JwtTokenStorage jwtTokenStorage;

    public CustomLogoutSuccessHandler(JwtTokenGenerator jwtTokenGenerator, JwtTokenStorage jwtTokenStorage) {
        this.jwtTokenGenerator = jwtTokenGenerator;
        this.jwtTokenStorage = jwtTokenStorage;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.hasText(header) && header.startsWith(AUTHENTICATION_PREFIX)) {
            String jwtToken = header.replace(AUTHENTICATION_PREFIX, "");
            destroyJwtToken(jwtToken, response);
        }
    }

    public void destroyJwtToken(String jwtToken, HttpServletResponse response) throws IOException {
        if (StringUtils.isEmpty(jwtToken)) {
            throw new RuntimeException("jwtToken is null");
        }
        JSONObject jsonObject = jwtTokenGenerator.decodeAndVerify(jwtToken);
        String aud = jsonObject.getStr("aud");
        if (!StringUtils.isEmpty(aud)) {
            jwtTokenStorage.expire(aud);
            JSONResult logout = JSONResult.success("logout", null);
            PrintWriter writer = response.getWriter();
            writer.println(JSONUtil.toJsonStr(logout));
            writer.flush();
            writer.close();
        }

        JSONResult logout = JSONResult.error("logout error", null);
        PrintWriter writer = response.getWriter();
        writer.println(JSONUtil.toJsonStr(logout));
        writer.flush();
        writer.close();
    }

    public String getCurrentUsername(Authentication authentication) {

        System.out.println(authentication);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }
        if (principal instanceof Principal) {
            return ((Principal) principal).getName();
        }
        return String.valueOf(principal);

    }
}
