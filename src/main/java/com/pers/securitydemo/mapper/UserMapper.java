package com.pers.securitydemo.mapper;

import com.pers.securitydemo.my.mapper.MyMapper;
import com.pers.securitydemo.model.pojo.User;

public interface UserMapper extends MyMapper<User> {
}