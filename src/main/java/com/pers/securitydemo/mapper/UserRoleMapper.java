package com.pers.securitydemo.mapper;

import com.pers.securitydemo.my.mapper.MyMapper;
import com.pers.securitydemo.model.pojo.UserRole;

public interface UserRoleMapper extends MyMapper<UserRole> {
}