package com.pers.securitydemo.mapper;

import com.pers.securitydemo.model.pojo.Url;
import com.pers.securitydemo.my.mapper.MyMapper;

/**
 * @author huagnjian
 */
public interface UrlMapper extends MyMapper<Url> {
}
