package com.pers.securitydemo.mapper;

import com.pers.securitydemo.my.mapper.MyMapper;
import com.pers.securitydemo.model.pojo.Role;

public interface RoleMapper extends MyMapper<Role> {
}