package com.pers.securitydemo.mapper;

import com.pers.securitydemo.model.pojo.Role;

import java.util.List;
import java.util.Set;

public interface RoleMapperCustom {

    List<Role> getByUserName(String userName);

    Set<Role> getByUrl(String url);
}
