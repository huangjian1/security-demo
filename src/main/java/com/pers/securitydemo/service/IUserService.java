package com.pers.securitydemo.service;


import com.pers.securitydemo.model.pojo.User;

public interface IUserService {

    /**
     * 根据name查user
     * @param userName 用户名
     * @return user
     */
    User getUserByName(String userName);

}
