package com.pers.securitydemo.service.impl;

import com.pers.securitydemo.mapper.RoleMapper;
import com.pers.securitydemo.mapper.RoleMapperCustom;
import com.pers.securitydemo.model.pojo.Role;
import com.pers.securitydemo.service.IRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements IRoleService {


    private final RoleMapperCustom roleMapperCustom;

    private final RoleMapper roleMapper;

    @Override
    public List<Role> getByUserName(String userName) {
        return roleMapperCustom.getByUserName(userName);
    }

    @Override
    public List<Role> findAll() {
        return roleMapper.selectAll();
    }

    @Override
    public Set<String> getByUrl(String url) {
        Set<Role> byUrl = roleMapperCustom.getByUrl(url);
        return byUrl.stream().map(Role::getRoleName).collect(Collectors.toSet());
    }
}
