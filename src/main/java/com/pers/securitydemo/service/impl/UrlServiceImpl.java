package com.pers.securitydemo.service.impl;

import com.pers.securitydemo.mapper.UrlMapper;
import com.pers.securitydemo.model.pojo.Url;
import com.pers.securitydemo.service.IUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author huagnjian
 */
@Service
public class UrlServiceImpl implements IUrlService {

    @Autowired
    private UrlMapper urlMapper;

    @Override
    public Set<Url> findAll() {
        return new HashSet<>(urlMapper.selectAll());
    }
}
