package com.pers.securitydemo.service.impl;

import cn.hutool.core.lang.Assert;
import com.pers.securitydemo.mapper.UserMapper;
import com.pers.securitydemo.model.pojo.User;
import com.pers.securitydemo.service.IUserService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

/**
 * @author 西柚3
 */
@Service
public class UserServiceImpl implements IUserService {

    private final UserMapper userMapper;

    public UserServiceImpl(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    @Override
    public User getUserByName(String userName) {
        Assert.notEmpty(userName);

        Example example = new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userName",userName);

        return userMapper.selectOneByExample(example);
    }
}
