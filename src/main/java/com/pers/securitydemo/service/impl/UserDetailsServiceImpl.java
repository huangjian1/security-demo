package com.pers.securitydemo.service.impl;

import com.pers.securitydemo.model.pojo.Role;
import com.pers.securitydemo.service.IRoleService;
import com.pers.securitydemo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserService userService;

    @Autowired
    private IRoleService roleService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.pers.securitydemo.model.pojo.User userByName = userService.getUserByName(username);
        List<Role> byUserName = roleService.getByUserName(username);
        return changeToUser(userByName, byUserName);
    }

    private UserDetails changeToUser(com.pers.securitydemo.model.pojo.User user, List<Role> roles) {
        List<GrantedAuthority> grantedAuthorities = roles.stream().map(r -> new SimpleGrantedAuthority(r.getRoleName())).collect(Collectors.toList());
        UserDetails userDetails = new User(user.getUserName(), user.getPwd(), grantedAuthorities);
        return userDetails;
    }

}
