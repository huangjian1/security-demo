package com.pers.securitydemo.service.impl;

import com.pers.securitydemo.enmus.LoginTypeEnum;
import com.pers.securitydemo.service.LoginPostProcessor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY;
import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

public class FormLoginPostProcessor implements LoginPostProcessor {
    @Override
    public LoginTypeEnum getLoginTypeEnum() {

        return LoginTypeEnum.FORM;
    }

    @Override
    public String obtainUsername(ServletRequest request) {
        return request.getParameter(SPRING_SECURITY_FORM_USERNAME_KEY);
    }

    @Override
    public String obtainPassword(ServletRequest request) {
        return request.getParameter(SPRING_SECURITY_FORM_PASSWORD_KEY);
    }

}
