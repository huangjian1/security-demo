package com.pers.securitydemo.service;

import com.pers.securitydemo.model.pojo.Url;

import java.util.List;
import java.util.Set;

/**
 * @author huagnjian
 */
public interface IUrlService {

    /**
     * 获取所有url
     *
     * @return
     */
    Set<Url> findAll();
}
