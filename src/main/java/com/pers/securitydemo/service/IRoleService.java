package com.pers.securitydemo.service;

import com.pers.securitydemo.model.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface IRoleService {

    /**
     * 根据用户名查找用户拥有的权限
     * @param userName 用户名
     * @return roles
     */
    List<Role> getByUserName(@Param("userName") String userName);

    /**
     * find all role
     *
     * @return
     */
    List<Role> findAll();

    /**
     * 通过匹配url获取role
     *
     * @param url
     * @return
     */
    Set<String> getByUrl(String url);
}
