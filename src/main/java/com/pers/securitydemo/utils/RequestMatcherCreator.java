package com.pers.securitydemo.utils;

import com.pers.securitydemo.model.pojo.Url;
import org.springframework.security.web.util.matcher.RequestMatcher;

import java.util.Set;

public interface RequestMatcherCreator {

    /**
     * 转换为 reqMatcher
     *
     * @param metaResources metaResource
     * @return  reqMatcher
     */
    Set<RequestMatcher> convertToRequestMatcher(Set<Url> metaResources);


}