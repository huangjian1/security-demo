package com.pers.securitydemo.filters;

import cn.hutool.core.lang.Assert;
import com.pers.securitydemo.enmus.LoginTypeEnum;
import com.pers.securitydemo.service.LoginPostProcessor;
import com.pers.securitydemo.service.impl.FormLoginPostProcessor;
import com.pers.securitydemo.service.impl.JsonLoginPostProcessor;
import com.pers.securitydemo.utils.ParameterRequestWrapper;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_PASSWORD_KEY;
import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

/**
 * 前置通知拦截器用户获取与传递用户名与密码
 *
 * @author huangjian
 */
public class PreLoginFilter extends GenericFilterBean {

    //登陆类型
    private static final String LOGIN_TYPE_KEY = "login_type";


    private RequestMatcher requiresAuthenticationRequestMatcher;

    //一个登陆类型对应一个实现类并put到map中，通过loginType寻找实现类
    private Map<LoginTypeEnum, LoginPostProcessor> processors = new HashMap<>();


    //初始化 将LoginPostProcessor实现类的
    public PreLoginFilter(String loginProcessingUrl, Collection<LoginPostProcessor> loginPostProcessors) {
        Assert.notNull(loginProcessingUrl, "loginProcessingUrl must not be null");
        requiresAuthenticationRequestMatcher = new AntPathRequestMatcher(loginProcessingUrl, "POST");

        LoginPostProcessor loginPostProcessor = new FormLoginPostProcessor();
        LoginPostProcessor jsonPostProcessor = new JsonLoginPostProcessor();
        processors.put(loginPostProcessor.getLoginTypeEnum(), loginPostProcessor);
        processors.put(jsonPostProcessor.getLoginTypeEnum(), jsonPostProcessor);

        //构造对象时如果有传入集合则put到map中
        if (!CollectionUtils.isEmpty(loginPostProcessors)) {
            loginPostProcessors.forEach(element -> processors.put(element.getLoginTypeEnum(), element));
        }

    }

    //通过loginType获取LoginTypeEnum
    private LoginTypeEnum getTypeFromReq(ServletRequest request) {
        String parameter = request.getParameter(LOGIN_TYPE_KEY);

        int i = Integer.parseInt(parameter);
        LoginTypeEnum[] values = LoginTypeEnum.values();
        return values[i];
    }


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        com.pers.securitydemo.utils.ParameterRequestWrapper parameterRequestWrapper = new ParameterRequestWrapper((HttpServletRequest) request);
        if (requiresAuthenticationRequestMatcher.matches((HttpServletRequest) request)) {

            //通过LOGIN_TYPE_KEY获取LoginTypeEnum
            LoginTypeEnum typeFromReq = getTypeFromReq(request);

            //通过LoginTypeEnum在map中getLoginPostProcessor
            LoginPostProcessor loginPostProcessor = processors.get(typeFromReq);

            //在loginPostProcessor中获取username
            String username = loginPostProcessor.obtainUsername(request);

            //在loginPostProcessor中获取password
            String password = loginPostProcessor.obtainPassword(request);

            //参数放入attribute中
            parameterRequestWrapper.setAttribute(SPRING_SECURITY_FORM_USERNAME_KEY, username);
            parameterRequestWrapper.setAttribute(SPRING_SECURITY_FORM_PASSWORD_KEY, password);

        }
        //执行下一个拦截器
        chain.doFilter(parameterRequestWrapper, response);
    }

}

