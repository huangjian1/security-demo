package com.pers.securitydemo.filters;

import com.pers.securitydemo.service.IRoleService;
import com.pers.securitydemo.service.IUrlService;
import com.pers.securitydemo.utils.RequestMatcherCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Set;

/**
 * @author huagnjian
 */
public class MyFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    @Autowired
    private IUrlService urlService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    private RequestMatcherCreator requestMatcherCreator;

    /**
     * 根据url获取该url配置的所有角色
     *
     * @param object
     * @return
     * @throws IllegalArgumentException
     */
    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        // 获取当前的请求
        final HttpServletRequest request = ((FilterInvocation) object).getRequest();
        //获取数据库中已经配置完成的url
        Set<RequestMatcher> requestMatchers = requestMatcherCreator.convertToRequestMatcher(urlService.findAll());
        //在requestMatchers中找到与请求匹配的url，若不存在则抛出异常
        RequestMatcher reqMatcher = requestMatchers.stream().filter(requestMatcher -> requestMatcher.matches(request)).findAny().orElseThrow(() -> new AccessDeniedException("非法访问"));
        //转换成AntPathRequestMatcher对象，更方便使用url
        AntPathRequestMatcher antPathRequestMatcher = (AntPathRequestMatcher) reqMatcher;
        //获取url
        String pattern = antPathRequestMatcher.getPattern();
        //通过url去查找拥有这些资源的role
        Set<String> roles = roleService.getByUrl(pattern);
        //roles是否为null，不为null则转换为list返回
        return CollectionUtils.isEmpty(roles) ? null : SecurityConfig.createList(roles.toArray(new String[0]));
    }

    /**
     * 获取所有角色
     *
     * @return
     */
    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    /**
     * 对特定的安全对象是否提供 ConfigAttribute 支持
     *
     * @param clazz
     * @return
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
    }
}
